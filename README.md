# molvis

A simple python module for creating html/js files that use [3dmol.js](https://3dmol.csb.pitt.edu/) to visualise structures

## Installation

To install, use `pip`


```shell

pip install molvis

```

## Documentation
Documentation for this package can be found here, and documentation for 3dmol.js can be found [here](https://3dmol.csb.pitt.edu/)
